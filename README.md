# samtools Singularity container
### Package samtools Version 1.14
Tools for dealing with SAM, BAM and CRAM files
Citing
Twelve years of SAMtools and BCFtools
Petr Danecek, James K Bonfield, Jennifer Liddle, John Marshall, Valeriu Ohan, Martin O Pollard, Andrew Whitwham, Thomas Keane, Shane A McCarthy, Robert M Davies, Heng Li
GigaScience, Volume 10, Issue 2, February 2021, giab008, https://doi.org/10.1093/gigascience/giab008


Homepage:

https://github.com/samtools/samtools

Package installation using Miniconda3 V4.12.0
All packages are in /opt/miniconda/bin & are in PATH
samtools Version: 1.14<br>
Singularity container based on the recipe: Singularity.samtools_v1.14.def

Local build:
```
sudo singularity build samtools_v1.14.sif Singularity.samtools_v1.14.def
```

Get image help:
```
singularity run-help samtools_v1.14.sif
```

Default runscript: samtools<br>
Usage:
```
./samtools_v1.14.sif --help
```
or:
```
singularity exec samtools_v1.14.sif samtools --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull samtools_v1.14.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/samtools/samtools:latest

```

